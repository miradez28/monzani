<?php
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\SMTP;
	use PHPMailer\PHPMailer\Exception;

	require 'PHPMailer/Exception.php';
	require 'PHPMailer/PHPMailer.php';
	require 'PHPMailer/SMTP.php';

	if (empty($_POST['nombre']) && strlen($_POST['nombre']) == 0 || empty($_POST['telefono']) && strlen($_POST['telefono']) == 0 || empty($_POST['email']) && strlen($_POST['email']) == 0 || empty($_POST['mensaje_17126']) && strlen($_POST['mensaje_17126']) == 0)
	{
		return false;
	}
	
	$nombre = $_POST['nombre'];
	$telefono = $_POST['telefono'];
	$email = $_POST['email'];
	$servicios = $_POST['servicios'];
	$sucursal = $_POST['sucursal'];
	$mensaje_17126 = $_POST['mensaje_17126'];

	/** SMTP Server Credentials **/
	$smtp_host = 'mail.monzani.com.mx';
	$smtp_username = 'contacto@monzani.mx';
	$smtp_password = 'uy*VY]3bMy&%';
	$smtp_port = '465';

	// Create Message	
	$to = 'contacto@monzani.mx,hola@manuelrosique.com,gerencia@monzani.mx';
	$replyTo = $email;
	$email_subject = "Mensaje enviado desde Monzani";
	$email_body = "Has recibido un nuevo mensaje. <br><br>Nombre: $nombre <br>Telefono: $telefono <br>Email: $email <br>Servicios: $servicios <br>Sucursal: $sucursal <br>Mensaje_17126: $mensaje_17126 <br>";

	$mail = new PHPMailer(true);

	try {
		$mail->isSMTP();
		$mail->CharSet = 'utf-8';
		$mail->Host = $smtp_host;
		$mail->SMTPAuth = true;
		$mail->Username = $smtp_username;
		$mail->Password = $smtp_password;
		$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; /** or PHPMailer::ENCRYPTION_SMTPS for SSL **/
		$mail->Port = $smtp_port;

		$mail->setFrom($smtp_username, 'Monzani');
		$mail->addAddress($to);
    	$mail->addReplyTo($replyTo, 'Reply To');

		$mail->isHTML(true);
		$mail->Subject = $email_subject;
		$mail->Body = $email_body;

		$mail->send();
		exit;
	}
	catch (Exception $e)
	{
		$error = array("message" => 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo);
		header('Content-Type: application/json');
		http_response_code(500);
		echo json_encode($error);
	}
?>

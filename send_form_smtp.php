<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';

/** Replace these with your SMTP server credentials **/
$smtp_host = 'mail.monzani.com.mx';
$smtp_username = 'contacto@monzani.mx';
$smtp_password = 'uy*VY]3bMy&%';
$smtp_port = 587;  /** or 465 for SSL **/

/** Recipient email address **/
$to = 'contacto@monzani.mx';

/** Form field names and the email subject **/
$form_fields = array(
	'nombre' => 'Nombre',
	'telefono' => 'Telefono',
	'servicios' => 'Servicios',
	'sucursal' => 'Sucursal',
	'email' => 'Email',
	'mensaje' => 'Mensaje'
);
$email_subject = 'Mensaje de Monzani.com';

/** Thank you page URL **/
$thank_you_page = 'https://monzani.com.mx/thank-you/';

$mail = new PHPMailer(true);

try {
	$mail->isSMTP();
	$mail->CharSet = 'utf-8';
	$mail->Host = $smtp_host;
	$mail->SMTPAuth = true;
	$mail->Username = $smtp_username;
	$mail->Password = $smtp_password;
	$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; /** or PHPMailer::ENCRYPTION_SMTPS for SSL **/
	$mail->Port = $smtp_port;

	$mail->setFrom($smtp_username, 'monzani.mx');
	$mail->addAddress($to);

	$mail->isHTML(true);
	$mail->Subject = $email_subject;

	$body = '';
	foreach ($form_fields as $field => $label) {
		$body .= '<p><strong>' . $label . ':</strong> ' . $_POST[$field] . '</p>';
	}
	$mail->Body = $body;

	$mail->send();
	header("Location: $thank_you_page");
	exit;
} catch (Exception $e) {
	echo 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo;
}
?>
